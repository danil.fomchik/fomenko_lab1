public class Eagle extends Bird{
    public Eagle(String type, String color, BirdSize birdSize) {
        super(type, color, birdSize);
    }

    @Override
    public void swim() {

    }


    @Override
    public void run() {

    }

    @Override
    public void cry() {

    }

    @Override
    public void fly() {

    }
}
