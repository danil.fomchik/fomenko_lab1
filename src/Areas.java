public enum Areas {

    BAT_AREAS("caves"),
    DUCK_AREAS("ponds"),
    EAGLE_AREAS("deserts"),
    KIWI_AREAS("forests"),
    OSTRICH_AREAS("savannas"),
    PENGUIN_AREAS("antarctica"),
    SWALLOW_AREAS("cities");

    public final String area;

    Areas(String area){
        this.area = area;
    }
}
