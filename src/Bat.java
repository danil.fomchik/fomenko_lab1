public class Bat extends Bird{

    public Bat(String type, String color, BirdSize birdSize) {
        super(type, color, birdSize);
    }

    @Override
    public void swim() {
        System.out.println(super.getType() + " are swimming");
    }


    @Override
    public void run() {

    }

    @Override
    public void cry() {

    }

    @Override
    public void fly() {

    }
}
