public enum BirdsType {

    //ТИПЫ ПТИЦ ПРЕДСТАВЛЕНЫ В ВИДЕ ПЕРЕЧИСЛЕНИЯ
    BAT("bat"),
    DUCK("duck"),
    EAGLE("eagle"),
    KIWI("kiwi"),
    OSTRICH("ostrich"),
    PENGUIN("penguin"),
    SWALLOW("swallow");

    public final String type;

    BirdsType(String type){
        this.type = type;
    }
}
