import java.util.*;

public class Activity {
    private Map<BirdsType, Set<String>> foodMap = new HashMap<>();
    private Map<String, BirdSize> sizesOfBirdsMap = new HashMap<>();
    private Map<String, BirdsType> typesOfBirdsMap = new HashMap<>();
    private Map<String, Actions> actionsOfBirdsMap = new HashMap<>();

    Activity(){
        setDefaultMapOfBirdSizes();
        setDefaultFoodMap();
        setDefaultMapOfBirdTypes();
        setDefaultMapOfBirdActions();
    }
    private void setDefaultMapOfBirdSizes(){
        sizesOfBirdsMap.put(BirdSize.VERY_SMALL.size, BirdSize.VERY_SMALL);
        sizesOfBirdsMap.put(BirdSize.SMALL.size, BirdSize.SMALL);
        sizesOfBirdsMap.put(BirdSize.MIDDLE.size, BirdSize.MIDDLE);
        sizesOfBirdsMap.put(BirdSize.BIG.size, BirdSize.BIG);
        sizesOfBirdsMap.put(BirdSize.VERY_BIG.size, BirdSize.VERY_BIG);
    }

    private void setDefaultMapOfBirdTypes(){
        typesOfBirdsMap.put(BirdsType.BAT.type, BirdsType.BAT);
        typesOfBirdsMap.put(BirdsType.DUCK.type, BirdsType.DUCK);
        typesOfBirdsMap.put(BirdsType.EAGLE.type, BirdsType.EAGLE);
        typesOfBirdsMap.put(BirdsType.KIWI.type, BirdsType.KIWI);
        typesOfBirdsMap.put(BirdsType.OSTRICH.type, BirdsType.OSTRICH);
        typesOfBirdsMap.put(BirdsType.PENGUIN.type, BirdsType.PENGUIN);
        typesOfBirdsMap.put(BirdsType.SWALLOW.type, BirdsType.SWALLOW);
    }

    private void setDefaultMapOfBirdActions(){
        actionsOfBirdsMap.put(Actions.RUN.type, Actions.RUN);
        actionsOfBirdsMap.put(Actions.CRY.type, Actions.CRY);
        actionsOfBirdsMap.put(Actions.FLY.type, Actions.FLY);
        actionsOfBirdsMap.put(Actions.SWIM.type, Actions.SWIM);
        actionsOfBirdsMap.put(Actions.EAT.type, Actions.EAT);


    }
    public boolean checkIfBirdSizeCorrect(String size){
        if (sizesOfBirdsMap.containsKey(size)){
            return true;
        } else return false;
    }

    public boolean checkIfBirdTypeCorrect(String type){
        if (typesOfBirdsMap.containsKey(type)){
            return true;
        } else return false;
    }

    public boolean checkIfBirdActionCorrect(String type){
        if (actionsOfBirdsMap.containsKey(type)){
            return true;
        } else return false;
    }

    /*
    Метод принимает тип, цвет и размер птицы и также принимает действие
    В методе создаем птицу с переданными парметрами и вызываем действие у птицы
    Действие птицы выведет сообщение
    */
    public void startActivity(String birdsType, String color, String size, Actions action) {
        Bird bird = BirdsFactory.createBird(birdsType, color, sizesOfBirdsMap.get(size));

        switch (action) {
            case RUN:
                bird.run();
                break;
            case CRY:
                bird.cry();
                break;
            case FLY:
                bird.fly();
                break;
            case SWIM:
                bird.swim();
                break;
            case EAT:
                int hashSetSize = foodMap.get(typesOfBirdsMap.get(birdsType)).size();
                System.out.println(bird.eat() + foodMap.get(typesOfBirdsMap.get(birdsType)).toArray()[(int)(Math.random() * hashSetSize-1)]);
                break;
        }
    }

    public void setDefaultFoodMap() {
        Set<String> batFood = new HashSet<>();
        batFood.addAll(Arrays.asList("insects,fruits,nectar,pollen".split(",")));
        Set<String> duckFood = new HashSet<>();
        duckFood.addAll(Arrays.asList("sweetcorn,lettuce,peas,oats,seeds,rice".split(",")));
        Set<String> eagleFood = new HashSet<>();
        eagleFood.addAll(Arrays.asList("fish,small birds,reptiles,rabbits,ground squirrels,bighorn sheep".split(",")));
        Set<String> kiwiFood = new HashSet<>();
        kiwiFood.addAll(Arrays.asList("woodlice,millipedes,centipedes,slugs,snails,spiders,insects,seeds,berries,plant material".split(",")));
        Set<String> ostrichFood = new HashSet<>();
        ostrichFood.addAll(Arrays.asList("plant roots,plant seeds,flora,sprouts,berries,nuts,succulents,mice,rats,snakes".split(",")));
        Set<String> penguinFood = new HashSet<>();
        penguinFood.addAll(Arrays.asList("fish,krill,squids".split(",")));
        Set<String> swallowFood = new HashSet<>();
        swallowFood.addAll(Arrays.asList("flies,grasshoppers,crickets,dragonflies,beetles".split(",")));
        foodMap.put(BirdsType.BAT, batFood);
        foodMap.put(BirdsType.DUCK, duckFood);
        foodMap.put(BirdsType.EAGLE, eagleFood);
        foodMap.put(BirdsType.KIWI, kiwiFood);
        foodMap.put(BirdsType.OSTRICH, ostrichFood);
        foodMap.put(BirdsType.PENGUIN, penguinFood);
        foodMap.put(BirdsType.SWALLOW, swallowFood);
    }
}