public interface BirdsSkills {
    //используем интерфейс как контракт между классами и интерфейсом, в котором классы должны реализовать методы интерфейса

    void swim();
    String eat();
    void run();
    void cry();
    void fly();

}