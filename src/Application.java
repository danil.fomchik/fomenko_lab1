import java.util.Arrays;
import java.util.Scanner;

public class Application {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            printMainMenu();
            switch (chooseApp()) {
                case 1:
                    runBirdApp();
                    break;
                case 2:
                    runEmployeesApp();
                    break;
                case 0:
                    System.out.println("Exit from application");
                    break;
            }
        }
    }
    public static void printMainMenu(){
        System.out.println("1 - to run birds application");
        System.out.println("2 - to run employees application");
        System.out.println("0 - to stop program");
    }

    private static void runBirdApp() {

        Activity activity = new Activity();

        System.out.println("Enter birds type from list below");

        //Считываем все типы птиц и выводим их пользователю для выбора типа
        Arrays.asList(BirdsType.values())
                .stream()
                .forEach(type -> System.out.println(type.type));
        String birdsType = scanner.next();

        System.out.println("Enter color");
        String color = scanner.next();

        System.out.println("Enter size from list below");
        Arrays.asList(BirdSize.values())
                .stream()
                .forEach(size -> System.out.println(size.size));
        String size = scanner.next();

        System.out.println("Enter action from list below");
        Arrays.asList(Actions.values())
                .stream()
                .forEach(actions -> System.out.println(actions.type));
        String action = scanner.next();

        if (activity.checkIfBirdSizeCorrect(size)
                && activity.checkIfBirdTypeCorrect(birdsType)
                && activity.checkIfBirdActionCorrect(action))
        {
            activity.startActivity(birdsType, color, size, Actions.valueOf(action.toUpperCase()));
        } else {
            System.out.println("Error, you entered wrong parameters");
        }
    }



    private static void runEmployeesApp() {
        System.out.println("Choose 1 to create contract employee or 2 to create salaried employee");
        String typeOfWork = readTypeOfWork();
        System.out.println("Enter 1 to create Contract Employee or enter 2 to create Salaried Employee");
        int typeOfCreation = scanner.nextInt();
        switch (typeOfCreation) {

            //ContractEmployee
            case 1:
                Employee employee = new Employee_On_Contract("1", typeOfWork, 5);

                break;
            //SalariedEmployeecase 2:
            case 2:

                break;
            default:
                System.out.println("Choose 1 or 2 ()");
        }
    }

    private static String readTypeOfWork(){
        System.out.println("Choose type of work - 1 for remote or 2 for office");
        int answer = scanner.nextInt();
        if (answer == 1){
            return "office";
        } else if (answer == 2){
            return "remote";
        } else return "error";
    }

    private static int chooseApp() {
        int choice = scanner.nextInt();
        if (choice == 1) {
            return 1;
        } else if (choice == 2) {
            return 2;
        } else if (choice == 0) {
            return 0;
        } else {
            return -1;
        }
    }
}
