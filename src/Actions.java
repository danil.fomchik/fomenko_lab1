public enum Actions {
    RUN("run"), CRY("cry"), FLY("fly"), SWIM("swim"), EAT("eat");

    public final String type;

    Actions(String type){
        this.type = type;
    }
}
