public class Employee_On_Salary implements Employee {
    private String ID;
    private String form_Of_Work;
    private String salary_Message;
    private int salary;
    public Employee_On_Salary(String ID, String form_Of_Work, String salary_Message) {
        this.ID = ID;
        this.form_Of_Work = form_Of_Work;
        this.salary_Message = salary_Message;
    }

    public void calculatePay(){

    }

    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getSalary() {
        return salary_Message;
    }

    @Override
    public String get_Form_Of_Work() {
        return form_Of_Work;
    }
}
