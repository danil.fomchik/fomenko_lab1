public interface Employee {
    String getID();
    String getSalary();
    String get_Form_Of_Work();
    void calculatePay();
}
