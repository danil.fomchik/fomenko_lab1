public abstract class Bird implements BirdsSkills{
    private String type;
    private String color;
    private BirdSize birdSize;

    public Bird(String type, String color, BirdSize birdSize) {
        this.type = type;
        this.color = color;
        this.birdSize = birdSize;
    }

    public String getType() {
        return type;
    }

    public BirdSize getBirdSize() {
        return birdSize;
    }

    public String getColor() {
        return color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setBirdSize(BirdSize birdSize) {
        this.birdSize = birdSize;
    }

    @Override
    public String eat() {
        return getType() + " is eating ";
    }
}
