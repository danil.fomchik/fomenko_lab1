public class BirdsFactory {

    public static Bird createBird(String birdsType, String color, BirdSize size) {

        BirdsType birdsTypes = BirdsType.valueOf(birdsType.toUpperCase());

        switch (birdsTypes) {

            case BAT:
                return new Bat(birdsType, color, size);

            case DUCK:
                return new Duck(birdsType, color, size);

            case EAGLE:
                return new Eagle(birdsType, color, size);

            case KIWI:
                return new Kiwi(birdsType, color, size);

            case OSTRICH:
                return new Ostrich(birdsType, color, size);

            case PENGUIN:
                return new Penguin(birdsType, color, size);

            case SWALLOW:
                return new Swallow(birdsType, color, size);

            default:
                throw new IllegalStateException("Unexpected value: " + birdsType);
        }
    }
}
