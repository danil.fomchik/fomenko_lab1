public enum BirdSize {

    //РАЗМЕРЫ ПТИЦ ПРЕДСТАВЛЕНЫ В ВИДЕ ПЕРЕЧИСЛЕНИЯ
    VERY_SMALL("very small"),
    SMALL("small"),
    MIDDLE("middle"),
    BIG("big"),
    VERY_BIG("very big");

    public final String size;

    BirdSize(String size){
        this.size = size;
    }
}
