import javax.swing.plaf.synth.SynthLookAndFeel;

public class Employee_On_Contract implements Employee{
    private String ID;
    private String typeOfWork;
    private String salary_Message;
    private double salary;
    private int agesOfExperience;

    public Employee_On_Contract(String ID, String typeOfWork, int agesOfExperience) {
        this.ID = ID;
        this.typeOfWork = typeOfWork;
        this.agesOfExperience = agesOfExperience;
    }

    public void calculatePay(){
        double coefOfWorkForm = typeOfWork == "office" ? 1.0 : 1.7;
        this.salary = (5+Math.random()*10)*(agesOfExperience*0.7)*coefOfWorkForm;
    }
    private void setDefaultSalary_Message(){
        this.salary_Message = "" + salary;
    }




    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getSalary() {
        return salary_Message;
    }

    @Override
    public String get_Form_Of_Work() {
        return typeOfWork;
    }
}
